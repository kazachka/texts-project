import $ from 'jquery';

function apiRequest(params) {
    return new Promise((resolve, reject) => $.ajax(params)
        .done((data) => {
            resolve(data);
        })
        .fail(() => reject())
    );
}

export function deleteTextById(id) {
    return apiRequest({
        method: "POST",
        url: "/api/text/deleteById",
        contentType: "application/json",
        data: JSON.stringify({id})
    });
}

export function createText(text, tagsId) {
    apiRequest({
        method: "POST",
        url: "/api/text/create",
        contentType: "application/json",
        data: JSON.stringify({text, tagsId})
    });
}

export function getAllTexts() {
    return apiRequest({url: "/api/text/getAll"});
}

export function getAllTags() {
    return apiRequest({url: "/api/tag/getAll"});
}

export function updateText(id, text, tagsId) {
    apiRequest({
        method: "POST",
        url: "/api/text/updateById",
        contentType: "application/json",
        data: JSON.stringify({id, text, tagsId})
    });
}