import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {createText, getAllTags, updateText} from "./api";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import '../css/EditTextPage.css'

class EditTextPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text : this.props.location?.propsEdit?.body || "",
            setedTags: this.props.location?.propsEdit?.tags.map(t => t.id) || [],
            tags: [],
        };
    }

    componentDidMount() {
        getAllTags().then((tags) => this.setState({tags}))
    }

    render() {
        const handleChange = (event) => {
            this.setState({setedTags: event.target.value});
        };
        const textChange = (event) => {
            this.setState({text: event.target.value});
        };
        return(
            <Container>
                <form>
                    <div className="cancel-button">
                        <Button>
                            <Link to={"/"}>
                                Cancel
                            </Link>
                        </Button>
                    </div>
                    <div className="labels">
                        <Typography>
                            Text:
                        </Typography>
                    </div>
                    <TextField
                        multiline
                        fullWidth
                        margin="normal"
                        minRows={4}
                        maxRows={10}
                        value={this.state.text}
                        onChange={textChange}
                        variant="outlined"
                    />
                    <div className="labels">
                        <Typography>
                            Select Tags:
                        </Typography>
                    </div>
                    <Select
                        className="tag-select"
                        label="tags"
                        multiple={true}
                        onChange={handleChange}
                        value={this.state.setedTags}
                    >
                        {this.state.tags.map(t =>
                            <MenuItem key={t.id} value={t.id}>{t.name}</MenuItem>
                        )}
                    </Select>
                </form>
                <div className="save-button">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            this.props.location?.propsEdit != null
                                ? updateText(
                                        this.props.location?.propsEdit?.id,
                                        this.state.text,
                                        this.state.setedTags
                                    )
                                : createText(
                                        this.state.text,
                                        this.state.setedTags
                                    );
                            window.location.assign('/');
                        }}
                    >
                        Save
                    </Button>
                </div>
            </Container>
        );
    }
}

export default EditTextPage;