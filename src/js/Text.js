import React, {Component} from "react";
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import DeleteIcon from '@material-ui/icons/Delete';
import '../css/Text.css'

class Text extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noWrap: true,
        };
    }

    render() {
        let user = this.props.text.user?.name || '"deleted"'
        return(
            <Card className="text">
                <Typography>
                    Author: {user}
                </Typography>
                <Grid className="text-body text-buttons-container">
                    <Grid item xs={10}>
                        <Typography id={`text${this.props.text.id}`} noWrap={this.state.noWrap}>
                            {this.props.text.body}
                        </Typography>
                    </Grid>
                    <Grid item xs={2} className="text-buttons text-buttons-container">
                        {this.props.text.body.length > 150 &&
                        <Button
                            onClick={() => {
                                this.setState((previousState) => ({
                                    noWrap : !previousState.noWrap
                                }));
                            }}
                        >
                            ...
                        </Button>
                        }
                        <Button>
                            <Link to={{pathname: "/edit", propsEdit: this.props.text}}>
                                Edit
                            </Link>
                        </Button>
                        <Button
                            variant="contained"
                            onClick={() => {
                                this.props.onTextDelete(this.props.text.id);
                            }}
                        >
                            <DeleteIcon />
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        );
    }
}

export default Text;