import React, {Component} from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import TextsPage from "./TextsPage";
import EditTextPage from "./EditTextPage";

class App extends Component {
    render() {
        return(
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={TextsPage}/>
                    <Route path="/edit" component={EditTextPage}/>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App;