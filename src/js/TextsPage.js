import React, {Component} from "react";
import Text from "./Text";
import Container from "@material-ui/core/Container";
import {deleteTextById, getAllTexts} from "./api";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import '../css/TextsPage.css'

class TextsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };
    }

    componentDidMount() {
        getAllTexts().then((data) => this.setState({data}))
    }

    onTextDelete(id) {
        deleteTextById(id).then(() =>
            getAllTexts().then((data) => this.setState({data}))
        )
    }

    render() {
        return(
            <Container>
                <div className="create-button">
                    <Button
                        variant="contained"
                    >
                        <Link to={"/edit"}>
                            Create
                        </Link>
                    </Button>
                </div>
                {this.state.data.map(d =>
                    <Text key={d.id} text={d} onTextDelete={(id) => this.onTextDelete(id)} />
                )}
            </Container>
        );
    }
}

export default TextsPage;