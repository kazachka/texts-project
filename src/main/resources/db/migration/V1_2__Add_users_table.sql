CREATE TABLE `users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `login` CHAR(64) NOT NULL,
    `name` CHAR(64) NOT NULL,
    `email` CHAR(64) NOT NULL,
    `created_at` DATETIME NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `users` VALUES (1, 'test', 'test', 'test', '2020-01-01 11:11:11');