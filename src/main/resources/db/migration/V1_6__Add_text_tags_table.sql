ALTER TABLE `tags`
DROP FOREIGN KEY `tags_ibfk_1`,
DROP `text_id`;

CREATE TABLE `text tags` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `text_id` INT,
    `tag_id` INT,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`text_id`)
        REFERENCES `texts`(`id`)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (`tag_id`)
        REFERENCES `tags`(`id`)
        ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO `text tags` VALUES (1, 1, 1);