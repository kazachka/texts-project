CREATE TABLE `tags` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `date_created` DATETIME NOT NULL,
    `name` CHAR(64) NOT NULL,
    `text_id` INT,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`text_id`)
        REFERENCES `texts`(`id`)
        ON DELETE RESTRICT ON UPDATE CASCADE
);

INSERT INTO `tags` VALUES (1, '2020-01-01 11:11:11', 'tag1', 2);
