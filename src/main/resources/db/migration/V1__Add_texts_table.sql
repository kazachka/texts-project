CREATE TABLE `texts` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `date_created` DATETIME NOT NULL,
    `body` TEXT NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `texts` VALUES (1, '2020-01-01 11:11:11', 'test');
