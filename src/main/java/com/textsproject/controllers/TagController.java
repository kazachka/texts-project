package com.textsproject.controllers;

import com.textsproject.entity.dto.GetCountTagsDTO;
import com.textsproject.entity.dto.GetTagsForLastDayDTO;
import com.textsproject.entity.Tag;
import com.textsproject.model.TagService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {

    @Resource
    private TagService tagService;

    @GetMapping("getById")
    public Tag getTagById(@RequestParam int id) {
        return tagService.getTagById(id);
    }

    @GetMapping("getAll")
    public List<Tag> getAll() {
        return tagService.getAll();
    }

    @GetMapping("getTagsForLastDay")
    public List<GetTagsForLastDayDTO> getTagsForLastDay() {
        return tagService.getTagsForLastDay();
    }

    @GetMapping("getCountTagsForLastDay")
    public List<GetCountTagsDTO> getCountTagsForLastDay() {
        return tagService.getCountTagsForLastDay();
    }

}
