package com.textsproject.controllers;

import com.textsproject.entity.dto.GetCountUsersTextsDTO;
import com.textsproject.entity.User;
import com.textsproject.model.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("getById")
    public User getUserById(@RequestParam("id") int id) {
        return userService.getUserById(id);
    }

    @GetMapping("getCountTexts")
    public List<GetCountUsersTextsDTO> getCountTexts() {
        return userService.getCountTexts();
    }

    @GetMapping("count")
    public long getCountUsers() {
        return userService.getCountUsers();
    }

}
