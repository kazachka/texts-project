package com.textsproject.controllers;

import com.textsproject.entity.dto.CreateTextDTO;
import com.textsproject.entity.dto.GetFilteredTextDTO;
import com.textsproject.entity.Text;
import com.textsproject.entity.dto.UpdateTextDTO;
import com.textsproject.model.TextService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("api/text")
public class TextController {

    @Resource
    private TextService textService;

    @GetMapping
    public Text getText() {
        return textService.getText();
    }

    @PostMapping("updateById")
    public void updateById(@RequestBody UpdateTextDTO dto) {
        textService.updateById(dto);
    }

    @PostMapping("deleteById")
    public void deleteById(@RequestBody UpdateTextDTO dto) {
        textService.deleteTextById(dto.getId());
    }

    @PostMapping("create")
    public void createText(@RequestBody CreateTextDTO dto) {
        textService.createText(dto);
    }

    @GetMapping("getById")
    public Text getTextById(@RequestParam("id") int id) {
        return textService.getTextById(id);
    }

    @GetMapping("getAll")
    public List<Text> getAll() {
        return textService.getAll();
    }

    @GetMapping("getByUserLogin")
    public List<Text> getTextsByUserLogin(@RequestParam("login") String login) {
        return textService.getTextsByUserLogin(login);
    }

    @GetMapping("filtered")
    public List<Text> getFilteredTexts(@RequestBody GetFilteredTextDTO dto) {
        return textService.getFilteredTexts(dto);
    }

}
