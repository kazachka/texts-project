package com.textsproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Alias("Text")
public class Text {

    private int id;

    private Date dateCreated;

    private String body;

    private User user;

    private List<Tag> tags;

}
