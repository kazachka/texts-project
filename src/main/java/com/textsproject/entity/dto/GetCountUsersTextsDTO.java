package com.textsproject.entity.dto;

import lombok.Getter;
import org.apache.ibatis.type.Alias;

@Getter
@Alias("GetCountUsersTextsDTO")
public class GetCountUsersTextsDTO {

    private String login;

    private long count;

}
