package com.textsproject.entity.dto;

import lombok.Getter;
import org.apache.ibatis.type.Alias;

@Getter
@Alias("GetCountTagsDTO")
public class GetCountTagsDTO {

    private String tagName;

    private long count;

}
