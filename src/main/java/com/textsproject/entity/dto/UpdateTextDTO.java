package com.textsproject.entity.dto;

import lombok.Getter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Alias("UpdateTextDTO")
public class UpdateTextDTO {

    @NotNull
    @NotEmpty
    private int id;

    @NotNull
    @NotEmpty
    private String text;

    @NotNull
    private Set<Integer> tagsId;

}
