package com.textsproject.entity.dto;

import lombok.Getter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

@Getter
@Alias("GetFilteredTextDTO")
public class GetFilteredTextDTO {

    @NotNull
    @Positive
    private Date fromDate;

    @NotNull
    @Positive
    private Date toDate;

    @NotNull
    @NotEmpty
    private String text;

}
