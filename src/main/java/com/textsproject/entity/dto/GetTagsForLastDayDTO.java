package com.textsproject.entity.dto;

import com.textsproject.entity.Text;
import lombok.Getter;
import org.apache.ibatis.type.Alias;

@Getter
@Alias("GetTagsForLastDayDTO")
public class GetTagsForLastDayDTO {

    private String tagName;

    private Text text;

}
