package com.textsproject.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@AllArgsConstructor
@Alias("InsertTagsInTextByIdDTO")
public class InsertTagsInTextByIdDTO {

    @NotNull
    @NotEmpty
    private int id;

    @NotNull
    private Set<Integer> tagsId;

}
