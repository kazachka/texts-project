package com.textsproject;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
public class MyBatisConfiguration {

    @Value("${flyway.url}")
    private String url;

    @Value("${flyway.user}")
    private String user;

    @Value("${flyway.password}")
    private String password;

    @Bean
    public DataSource dataSource() {
        PooledDataSource ds = new PooledDataSource();

        ds.setDriver("com.mysql.jdbc.Driver");
        ds.setUrl(url);
        ds.setUsername(user);
        ds.setPassword(password);

        return ds;
    }


    @Bean("sqlSession")
    public SqlSessionTemplate createSqlSession() throws Exception {
        return new SqlSessionTemplate(createSqlSessionFactory(), ExecutorType.SIMPLE);
    }

    @Bean("sqlSessionFactory")
    public SqlSessionFactory createSqlSessionFactory() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setConfigLocation(new ClassPathResource("SqlMapConfig.xml"));
        factoryBean.setDataSource(dataSource());
        factoryBean.setMapperLocations(
                new ClassPathResource("db/mapping/TextMapping.xml"),
                new ClassPathResource("db/mapping/UserMapping.xml"),
                new ClassPathResource("db/mapping/TagMapping.xml"));
        return factoryBean.getObject();
    }

}
