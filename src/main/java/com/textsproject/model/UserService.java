package com.textsproject.model;

import com.textsproject.entity.User;
import com.textsproject.entity.dto.GetCountUsersTextsDTO;

import java.util.List;

public interface UserService {

    User getUserById(int id);

    User getUserByLogin(String login);

    List<GetCountUsersTextsDTO> getCountTexts();

    long getCountUsers();

}
