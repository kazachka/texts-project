package com.textsproject.model;

import com.textsproject.entity.Tag;
import com.textsproject.entity.dto.GetCountTagsDTO;
import com.textsproject.entity.dto.GetTagsForLastDayDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SqlMapTagDao extends BaseSqlMapDao implements TagDao {

    public Tag getTagById(int id) {
        return getSqlSession().selectOne("selectTagById", id);
    }

    public List<Tag> getAll() {
        return getSqlSession().selectList("selectAllTags");
    }

    public List<GetTagsForLastDayDTO> getTagsForLastDay() {
        return getSqlSession().selectList("selectTagsForLastDay");
    }

    public List<GetCountTagsDTO> getCountTagsForLastDay() {
        return getSqlSession().selectList("selectCountTagsForLastDay");
    }

}
