package com.textsproject.model;

import org.apache.ibatis.session.SqlSession;

import javax.annotation.Resource;

public class BaseSqlMapDao {

    @Resource
    private SqlSession sqlSession;

    protected SqlSession getSqlSession() {
        return sqlSession;
    }

}
