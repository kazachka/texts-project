package com.textsproject.model.service;

import com.textsproject.entity.Text;
import com.textsproject.entity.dto.CreateTextDTO;
import com.textsproject.entity.dto.GetFilteredTextDTO;
import com.textsproject.entity.dto.InsertTagsInTextByIdDTO;
import com.textsproject.entity.dto.UpdateTextDTO;
import com.textsproject.model.TextDao;
import com.textsproject.model.TextService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Service
@Transactional(readOnly = true)
public class TextServiceImpl implements TextService {

    @Resource
    private TextDao textDao;

    public Text getText() {
        return textDao.getText();
    }

    public Text getTextById(int id) {
        return textDao.getTextById(id);
    }

    public List<Text> getAll() {
        return textDao.getAll();
    }

    @Transactional
    public void updateById(UpdateTextDTO dto) {
        textDao.updateById(dto);
        textDao.deleteTextTagsById(dto.getId());
        insertTagsInTextById(dto.getId(), dto.getTagsId());
    }

    public List<Text> getTextsByUserLogin(String login) {
        return textDao.getTextsByUserLogin(login);
    }

    public List<Text> getFilteredTexts(GetFilteredTextDTO dto) {
        return textDao.getFilteredTexts(dto);
    }

    @Transactional
    public void createText(CreateTextDTO dto) {
        textDao.insertText(dto);
        insertTagsInTextById(dto.getId(), dto.getTagsId());
    }

    @Transactional
    public void deleteTextById(int id) {
        textDao.deleteTextTagsById(id);
        textDao.deleteTextById(id);
    }

    private void insertTagsInTextById(int id, Set<Integer> tagsId) {
        InsertTagsInTextByIdDTO dto = new InsertTagsInTextByIdDTO(id, tagsId);
        Optional.of(dto.getTagsId())
                .filter(Predicate.not(Collection::isEmpty))
                .ifPresent(d -> textDao.insertTextTagsById(dto));
    }

}
