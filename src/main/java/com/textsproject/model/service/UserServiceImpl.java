package com.textsproject.model.service;

import com.textsproject.entity.User;
import com.textsproject.entity.dto.GetCountUsersTextsDTO;
import com.textsproject.model.UserDao;
import com.textsproject.model.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService, UserDetailsService {

    @Resource
    private UserDao userDao;

    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    public User getUserByLogin(String login) {
        return userDao.getUserByLogin(login);
    }

    public List<GetCountUsersTextsDTO> getCountTexts() {
        return userDao.getCountTexts();
    }

    public long getCountUsers() {
        return userDao.getCountUsers();
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userDao.getUserByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }
}
