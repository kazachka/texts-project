package com.textsproject.model.service;

import com.textsproject.entity.Tag;
import com.textsproject.entity.dto.GetCountTagsDTO;
import com.textsproject.entity.dto.GetTagsForLastDayDTO;
import com.textsproject.model.TagDao;
import com.textsproject.model.TagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class TagServiceImpl implements TagService {

    @Resource
    private TagDao tagDao;

    public Tag getTagById(int id) {
        return tagDao.getTagById(id);
    }

    public List<Tag> getAll() {
        return tagDao.getAll();
    }

    public List<GetTagsForLastDayDTO> getTagsForLastDay() {
        return tagDao.getTagsForLastDay();
    }

    public List<GetCountTagsDTO> getCountTagsForLastDay() {
        return tagDao.getCountTagsForLastDay();
    }

}
