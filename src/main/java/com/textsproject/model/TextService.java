package com.textsproject.model;

import com.textsproject.entity.Text;
import com.textsproject.entity.dto.CreateTextDTO;
import com.textsproject.entity.dto.GetFilteredTextDTO;
import com.textsproject.entity.dto.UpdateTextDTO;

import java.util.List;

public interface TextService {

    Text getText();

    Text getTextById(int id);

    List<Text> getAll();

    void updateById(UpdateTextDTO dto);

    List<Text> getTextsByUserLogin(String login);

    List<Text> getFilteredTexts(GetFilteredTextDTO dto);

    void createText(CreateTextDTO dto);

    void deleteTextById(int id);

}
