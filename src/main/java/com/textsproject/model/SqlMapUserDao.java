package com.textsproject.model;

import com.textsproject.entity.User;
import com.textsproject.entity.dto.GetCountUsersTextsDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SqlMapUserDao extends BaseSqlMapDao implements UserDao {

    public User getUserById(int id) {
        return getSqlSession().selectOne("selectUserById", id);
    }

    public User getUserByLogin(String login) {
        return getSqlSession().selectOne("selectUserByLogin", login);
    }

    public List<GetCountUsersTextsDTO> getCountTexts() {
        return getSqlSession().selectList("selectCountTexts");
    }

    public long getCountUsers() {
        return getSqlSession().selectOne("selectCountUsers");
    }

}
