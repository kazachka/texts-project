package com.textsproject.model;

import com.textsproject.entity.Text;
import com.textsproject.entity.dto.CreateTextDTO;
import com.textsproject.entity.dto.GetFilteredTextDTO;
import com.textsproject.entity.dto.InsertTagsInTextByIdDTO;
import com.textsproject.entity.dto.UpdateTextDTO;

import java.util.List;

public interface TextDao {

    Text getText();

    void updateById(UpdateTextDTO dto);

    void insertTextTagsById(InsertTagsInTextByIdDTO dto);

    void deleteTextTagsById(int id);

    Text getTextById(int id);

    List<Text> getFilteredTexts(GetFilteredTextDTO model);

    List<Text> getTextsByUserLogin(String login);

    List<Text> getAll();

    void insertText(CreateTextDTO dto);

    void deleteTextById(int id);
}
