package com.textsproject.model;

import com.textsproject.entity.Tag;
import com.textsproject.entity.dto.GetCountTagsDTO;
import com.textsproject.entity.dto.GetTagsForLastDayDTO;

import java.util.List;

public interface TagService {

    Tag getTagById(int id);

    List<Tag> getAll();

    List<GetTagsForLastDayDTO> getTagsForLastDay();

    List<GetCountTagsDTO> getCountTagsForLastDay();

}
