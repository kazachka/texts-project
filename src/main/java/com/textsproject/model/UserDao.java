package com.textsproject.model;

import com.textsproject.entity.User;
import com.textsproject.entity.dto.GetCountUsersTextsDTO;

import java.util.List;

public interface UserDao {

    User getUserById(int id);

    List<GetCountUsersTextsDTO> getCountTexts();

    long getCountUsers();

    User getUserByLogin(String login);
}
