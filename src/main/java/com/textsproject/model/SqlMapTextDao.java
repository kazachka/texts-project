package com.textsproject.model;

import com.textsproject.entity.Text;
import com.textsproject.entity.dto.CreateTextDTO;
import com.textsproject.entity.dto.GetFilteredTextDTO;
import com.textsproject.entity.dto.InsertTagsInTextByIdDTO;
import com.textsproject.entity.dto.UpdateTextDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SqlMapTextDao extends BaseSqlMapDao implements TextDao {

    @Override
    public Text getText() {
        return getSqlSession().selectOne("selectText");
    }

    public Text getTextById(int id) {
        return getSqlSession().selectOne("selectTextById", id);
    }

    public void updateById(UpdateTextDTO dto) {
        getSqlSession().update("updateTextById", dto);
    }

    public void insertTextTagsById(InsertTagsInTextByIdDTO dto) {
        getSqlSession().insert("insertTextTags", dto);
    }

    public void deleteTextTagsById(int id) {
        getSqlSession().delete("deleteTextTagsById", id);
    }

    public void insertText(CreateTextDTO dto) {
        getSqlSession().insert("insertText", dto);
    }

    public List<Text> getAll() {
        return getSqlSession().selectList("selectAllTexts");
    }

    public List<Text> getFilteredTexts(GetFilteredTextDTO model) {
        return getSqlSession().selectList("selectFilteredTexts", model);
    }

    public List<Text> getTextsByUserLogin(String login) {
        return getSqlSession().selectList("selectTextsByUserLogin", login);
    }

    public void deleteTextById(int id) {
        getSqlSession().delete("deleteTextById", id);
    }

}
